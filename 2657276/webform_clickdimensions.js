/**
 * @file
 * JavaScript behaviors for the front-end display of webforms dimensions.
 */

(function ($) {

  'use strict';

  Drupal.behaviors.webform_clickdimensionsFieldsetSummaries = {
    attach: function (context) {
      // console.log("worked");.
      $('fieldset.webform-clickdimensions-form', context).drupalSetSummary(function (context) {
        if ($('.form-item-clickdimension-enabled input', context).is(':checked')) {
          return Drupal.t('Enabled');
        }
        else {
          return Drupal.t('Disabled');
        }
      });
    }
  };

})(jQuery);
