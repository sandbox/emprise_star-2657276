Description
-----------
This module has support for many addons that allows them to integrate 
to Microsoft Dynamics CRM ClickDimensions system. 
A webform can be configured in order to visitors send data 
to Microsoft Dynamics CRM with http://clickdimensions.com/ form.
The module allows configurations needed by Click Dimensions, 
such as URL to Form Post action.
Submissions from a webform are saved in Click Dimensions system database.

Requirements
------------
Drupal 7.x
WebForm module
See https://www.drupal.org/project/webform_clickdimensions 
for additional requirements.

Installation
------------
1. Copy the entire webform_clickdimensions directory 
the Drupal sites/all/modules directory.

2. Login as an administrator. Enable the module in the "Administer" -> "Modules"

3. Create a webform node at node/add/webform.

4. (Optional) Edit the settings under 
"Click Dimensions settings" -> "Enable Click Dimensions" 
  -> Enter "Account key" from Click Dimensions form to "Account key" field  
  -> Enter "Domain" from Click Dimensions form to "Domain" field
  -> Enter "Action url" from Click Dimensions form to "Form action url" field
  -> The FORM KEY of amy field in webform  should look like 
 field NAME in Click Dimensions form . 
 Please see Click Dimensions form 
 http://help.clickdimensions.com/steps-to-check-before-using-form-capture-records/

5. Debug mode should be used in development.
  
Support
-------
Please use the issue queue for filing bugs with this module at
http://drupal.org/project/issues/webform_clickdimensions
